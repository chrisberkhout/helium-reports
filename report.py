#!/usr/bin/python

import argparse
import json
from datetime import datetime
from decimal import ROUND_UP, Decimal


def raise_for_unknown_types(activities):
    known_types = set(["rewards_v2", "assert_location_v2", "payment_v2", "token_burn_v1"])
    actual_types = set([a["type"] for a in activities])
    unknown_types = actual_types - known_types
    if unknown_types:
        msg = f"Unknown Helium activity type(s): {', '.join(unknown_types)}"
        raise ValueError(msg)


def enrich(activities, prices):
    def dc_to_hnt(dc, height):
        price = next(filter(lambda p: p["block"] < height, prices))
        hnt_usd = Decimal(price["price"]) / Decimal("100000000")
        dc_usd = Decimal("0.00001")
        usd = Decimal(dc) * dc_usd
        hnt = usd / hnt_usd
        return hnt.quantize(Decimal(".00000001"), rounding=ROUND_UP)

    acc = []
    for activity in activities:
        isotime = datetime.fromtimestamp(activity["time"]).isoformat()

        rewards = Decimal(
            sum(
                [
                    Decimal(r["amount"]) / Decimal("100000000")
                    for r in activity.get("rewards", [])
                ]
            )
        )
        payments = Decimal(
            sum(
                [
                    Decimal(r["amount"]) / Decimal("100000000")
                    for r in activity.get("payments", [])
                ]
            )
        )
        fees_dc = sum([activity.get("fee", 0), activity.get("staking_fee", 0)])
        fees_hnt = (
            dc_to_hnt(fees_dc, activity["height"])
            if fees_dc != 0
            else Decimal("0.00000000")
        )

        enriched = activity.copy()
        enriched["isotime"] = isotime
        enriched["total"] = rewards - payments - fees_hnt
        enriched["total_ex_fees"] = rewards - payments
        enriched["total_fees"] = - fees_hnt

        acc.append(enriched)

    return acc


def with_balance(activities):
    acc = []
    for a in activities:
        result = a.copy()
        result["balance"] = (acc[-1]["balance"] + a["total"]) if acc else a["total"]
        acc.append(result)
    return acc


def aggregate(activities):
    def same_month(first, second):
        return first[0:7] == second[0:7]

    acc = [activities[0].copy()]
    for a in activities[1:]:
        if (
            a["type"].startswith("reward")
            and a["type"] == acc[-1]["type"]
            and same_month(a["isotime"], acc[-1]["isotime"])
        ):
            acc[-1]
            updated = a.copy()
            updated["total"] = acc[-1]["total"] + a["total"]
            updated["total_ex_fees"] = acc[-1]["total_ex_fees"] + a["total_ex_fees"]
            updated["total_fees"] = acc[-1]["total_fees"] + a["total_fees"]
            updated["hash"] = "[multiple]"
            acc[-1] = updated
        else:
            acc.append(a)
    return acc


def only_rewards(activities):
    return list(filter(lambda a: a["type"].startswith("reward"), activities))


def not_rewards(activities):
    return list(filter(lambda a: not a["type"].startswith("reward"), activities))


def print_report(activites):
    for a in activites:
        isotime = a["isotime"].ljust(21)
        hsh = a["hash"].ljust(45)
        typ = a["type"].ljust(20)
        total_ex_fees = "{0:.8f}".format(a["total_ex_fees"]).rjust(16)
        total_fees = "{0:.8f}".format(a["total_fees"]).rjust(16)
        if "balance" in a:
            balance = "{0:.8f}".format(a["balance"]).rjust(16)
        else:
            balance = ""
        print("".join([isotime, hsh, typ, total_ex_fees, total_fees, balance]))


def build_parser():
    parser = argparse.ArgumentParser(
        description="Report on data/activities.json, using data/prices.json."
    )
    subparsers = parser.add_subparsers(title="commands", dest="command")
    subparsers.add_parser("all", help="Account register report")
    subparsers.add_parser("compact", help="Account register report, compacted")
    subparsers.add_parser(
        "rewards-compact", help="Account register report, rewards only, compacted"
    )
    subparsers.add_parser("other", help="Account register report, non-rewards only")
    return parser


def main(command):
    with open("data/activities.json", "r") as file:
        activities = json.load(file)

    with open("data/prices.json", "r") as file:
        prices = json.load(file)

    raise_for_unknown_types(activities)
    enriched = enrich(reversed(activities), prices)

    if command == "all":
        print_report(with_balance(enriched))
    elif command == "compact":
        print_report(aggregate(with_balance(enriched)))
    elif command == "rewards-compact":
        print_report(aggregate(with_balance(only_rewards(enriched))))
    elif command == "other":
        print_report(not_rewards(enriched))
    else:
        raise ValueError("Unknown report")


parser = build_parser()
args = parser.parse_args()
main(args.command)
