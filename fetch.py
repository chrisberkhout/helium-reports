#!/usr/bin/python

import argparse
import json
import sys

import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry

retry_strategy = Retry(
    total=5,
    status_forcelist=[429, 500, 502, 503, 504],
    allowed_methods=["HEAD", "GET", "OPTIONS"],
)
adapter = HTTPAdapter(max_retries=retry_strategy)
http = requests.Session()
http.mount("https://", adapter)
http.mount("http://", adapter)


def build_parser():
    parser = argparse.ArgumentParser(
        description="Fetch Helium API data, combining all available pages."
    )
    parser.add_argument("url", metavar="URL", help="URL to fetch from")
    return parser


def fetch_page(url, cursor):
    r = http.get(f"{url}?cursor={cursor}")
    try:
        parsed = r.json()
    except json.decoder.JSONDecodeError:
        print(f"ERROR: Couldn't parse as JSON: '{r.text}'", file=sys.stderr)
        sys.exit(1)
    data = parsed.get("data", [])
    cursor = parsed.get("cursor")
    return (data, cursor)


def fetch_all(url):
    data = []
    cursor = ""
    while True:
        new_data, cursor = fetch_page(url, cursor)
        data.extend(new_data)
        print(f"Fetched {len(new_data)} items, next cursor: {cursor}", file=sys.stderr)
        if not cursor:
            break
    return data


parser = build_parser()
args = parser.parse_args()
data = fetch_all(args.url)
print(json.dumps(data, indent=2))
