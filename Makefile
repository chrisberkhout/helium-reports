help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

balance:            ## Get the current balance via helium-wallet
	helium-wallet --format json balance --address $(HELIUM_ADDRESS)

update:             ## Fetch a full copy of the data
	@if [[ ! -e "data/.git" ]]; then mkdir -p data; git init data; fi
	./fetch.py 'https://api.helium.io/v1/accounts/$(HELIUM_ADDRESS)/activity' > data/activities-new.json
	./fetch.py 'https://api.helium.io/v1/oracle/prices' > data/prices-new.json
	cat data/activities{,-new}.json | jq ".[]" | jq --slurp "unique_by(.hash)|sort_by(.time)|reverse" > data/activities-combined.json
	cat data/prices{,-new}.json | jq ".[]" | jq --slurp "unique_by(.block)|reverse" > data/prices-combined.json
	mv data/activities-combined.json data/activities.json
	mv data/prices-combined.json data/prices.json
	rm data/activities-new.json data/prices-new.json
	git -C data add .
	git -C data diff-index --quiet HEAD -- || git -C data commit --message='Automatic commit.'

all:                ## Account register report
	@./report.py all

compact:            ## Account register report, compacted
	@./report.py compact

rewards-compact:    ## Account register report, rewards only, compacted
	@./report.py rewards-compact

other:              ## Account register report, non-rewards only
	@./report.py other
