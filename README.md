# Helium reports

Scripts to fetch account data using the
[Helium Blockchain API](https://docs.helium.com/api/blockchain/introduction/)
and generate reports, in order to:
- List all transactions and amounts necessary to reconstruct an account's balance.
- Calculate monthly totals of HNT rewards.

## How to use it

Set the environment variable `HELIUM_ADDRESS`, for
[example](https://explorer.helium.com/accounts/13tCnGNAscct3HqnPEbuoGfkj8gPwFWppXLQY3yoPRAekKw99Ep):
```bash
export HELIUM_ADDRESS="13tCnGNAscct3HqnPEbuoGfkj8gPwFWppXLQY3yoPRAekKw99Ep"
```

List the available make targets:
```
make
```
```
balance:             Get the current balance via helium-wallet
update:              Fetch a full copy of the data
all:                 Account register report
compact:             Account register report, compacted
rewards-compact:     Account register report, rewards only, compacted
other:               Account register report, non-rewards only
```

Fetch the data:
```
make update
```
```
Initialized empty Git repository in /home/username/helium-reports/data/.git/
./fetch.py 'https://api.helium.io/v1/accounts/13tCnGNAscct3HqnPEbuoGfkj8gPwFWppXLQY3yoPRAekKw99Ep/activity' > data/activities.json
Fetched 0 items, next cursor: eyJtaW5fYmxvY2siOjk5ODMzNiwibWF4X2Jsb2NrIjoxMDA2MjYwLCJibG9jayI6MTAwNjIwMCwiYW5jaG9yX2Jsb2NrIjoxMDA2MjAwfQ
Fetched 70 items, next cursor: None
./fetch.py 'https://api.helium.io/v1/oracle/prices' > data/prices.json
Fetched 100 items, next cursor: eyJiZWZvcmUiOjEwMDUwMjB9
Fetched 100 items, next cursor: eyJiZWZvcmUiOjEwMDM3OTB9
...
Fetched 92 items, next cursor: None
git -C data add .
git -C data diff-index --quiet HEAD -- || git -C data commit --message='Automatic commit.'
fatal: bad revision 'HEAD'
[master (root-commit) e532d78] Automatic commit.
 2 files changed, 100127 insertions(+)
 create mode 100644 activities.json
 create mode 100644 prices.json
```

Generate a report:
```
make compact
```
```
2021-09-06T09:32:06  [multiple]                                   rewards_v2                0.27660369      0.00000000      0.27660369
2021-09-06T09:45:52  nirHWy3yOIR2V7aiCREZlq-DIdwfqp-NoS9mgiP2-lU  assert_location_v2        0.00000000     -0.02255948      0.25404421
2021-09-11T09:37:38  [multiple]                                   rewards_v2                2.45704556      0.00000000      2.71108977
```

## Notes

The number columns show total HNT excluding fees, total fees paid in HNT
(converted from DC assuming [implicit burn](https://docs.helium.com/blockchain/transaction-fees/)
and using the oracle price from the time of the transaction), and cumulative
total that should match the account balance (when all transactions are
included).

The compact reports group rewards transactions that are in the same month and location.

The `data/` directory is automatically version controlled with git.

The reporting script will fail if it finds a transaction of any type except
those that are supported:
- `rewards_v2`
- `assert_location_v2`
- `payment_v2`

## Alternatives

As of 2021-09-06, none of the data export [community tools](https://explorer.helium.com/tools)
were able to list all the transactions and amounts required to reconstruct an
account balance. The best alternatives focused on rewards.

- [Bobcat Diagnoser](https://www.bobcatminer.com/post/bobcat-diagnoser-user-guide)
  - For local diagnostics of Bobcat miners, not for account data export.
- [DeWi Rewards Report](https://etl.dewi.org/dashboard/11-rewards-report-for-an-account)
  - My attempt to use it only showed latest block used.
- [Fairspot CSV Export](https://www.fairspot.host/hnt-export-mining-tax)
  - Location assertion transactions not included.
  - Has rounding errors (`0.030550340000000002` instead of `0.03055034`).
  - No code avilable.
- [Helisum](https://helisum.com/)
  - Requires registration.
  - Location assertion transactions not included, focus is on rewards.
  - SaaS app, no code avilable.
- [helium-tax](https://davetapley.com/helium-tax/)
  - Transaction count without details, excludes location assertions.
  - Rounds to whole units of HNT.
  - Code available under MIT license.
- [MinerTax](https://www.minertax.com/HNT/usd/)
  - Only shows rewards.
  - Export doesn't cover the current year.
